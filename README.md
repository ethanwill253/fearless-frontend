To run this app:
-docker-compose build
-docker-compose up
Runs on localhost:3000

# Conference Go

Leverages Pexels API to fetch city images by pulling city name from conference instance.
Utilizes Open Weather API to pull real time weather data for conference locations.
Refactored app from Monolith to microservice architecture using pub/sub and RabbitMQ.
This app was created to set up conferences. Users can sign-up to attend a conference.
Users can look up available conferences and register for the conference of their choice.
Conferences, venue Locations, and participant Presentations can be added and updated.
Presentation microservice use RabbitMQ to sent emails to conference heads to approve or deny presentations.

# Routes

Home:
-/
Attendees:
-/attendees
-/attendees/new
Add Conference:
-/conferences/new
Add Location:
-/locations/new
Add Presentation:
-/presentations/new

# Data Creation

-Method PUT:
  -Locations:
    {
      "name": "BOK Center",
      "city": "Dallas",
      "room_count": 100,
      "state": "TX"
    }

  -Conferences:
    {
      "name": "Cox",
      "starts": "2023-06-14",
      "ends": "2023-06-16",
      "description": "Tech Summit on Artificial Intelligence & Robotics."
      "max_presentations": 70,
      "max_attendees": 10000,
      "location": 2
    }

  -Attendees:
    {
      "email": "dusky@exmaple.com",
      "name": "Dusky",
      "conference": "/api/conferences/1/"
    }

  -Presentations:
    {
      "presenter_name": "Joe",
      "company_name": "Joe inc.",
      "presenter_email": "joe@email.com",
      "title": "Create Something",
      "synopsis": "To prove it works."
    }