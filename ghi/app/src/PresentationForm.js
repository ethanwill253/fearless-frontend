import React from 'react';

class PresentationForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      presenterName: '',
      presenterEmail: '',
      companyName: '',
      title: '',
      synopsis: '',
      conferences: []
    };

    this.handlePresentNameChange = this.handlePresentNameChange.bind(this);
    this.handlePresentEmailChange = this.handlePresentEmailChange.bind(this);
    this.handleCompanyNameChange = this.handleCompanyNameChange.bind(this);
    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleSynopsisChange = this.handleSynopsisChange.bind(this);
    this.handleConferenceChange = this.handleConferenceChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handlePresentNameChange(event) {
    const value = event.target.value;
    this.setState({presenterName: value})
  }

  handlePresentEmailChange(event) {
    const value = event.target.value;
    this.setState({presenterEmail: value})
  }

  handleCompanyNameChange(event) {
    const value = event.target.value;
    this.setState({companyName: value})
  }

  handleTitleChange(event) {
    const value = event.target.value;
    this.setState({title: value})
  }

  handleSynopsisChange(event) {
    const value = event.target.value;
    this.setState({synopsis: value})
  }

  handleConferenceChange(event) {
    const value = event.target.value;
    this.setState({conference: value})
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    data.presenter_name = data.presenterName
    data.presenter_email = data.presenterEmail
    data.company_name = data.companyName
    delete data.presenterName
    delete data.presenterEmail
    delete data.companyName
    delete data.conferences
    console.log("Submit data: ",data);

    const selectTag = document.getElementById('conference');
    const conferenceId = selectTag.options[selectTag.selectedIndex].id;
    const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
          'Content-Type': 'application/json',
      },
    };

    const response = await fetch(presentationUrl, fetchConfig);
    if (response.ok) {
      const newPresentation = await response.json();
      console.log("New Presentation data: ",newPresentation);
      const cleared = {
        presenterName: '',
        presenterEmail: '',
        companyName: '',
        title: '',
        synopsis: '',
        conferences: []
      };
      this.setState(cleared);
    }
  }

  async componentDidMount() {
    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      this.setState({conferences: data.conferences})
        // const selectTag = document.getElementById('location');
        // for (let location of data.locations) {
        //     const option = document.createElement('option')
        //     option.innerHTML = location['name']
        //     option.value = location['id']
        //     selectTag.appendChild(option)
        // }
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={this.handleSubmit} id="create-presentation-form">
            <div className="form-floating mb-3">
                <input onChange={this.handlePresentNameChange} placeholder="Presenter name" value={this.state.presenterName} required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handlePresentEmailChange} placeholder="Presenter email"  value={this.state.presenterEmail} required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleCompanyNameChange} placeholder="Company name" type="text"  value={this.state.companyName} name="company_name" id="company_name" className="form-control"/>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleTitleChange} placeholder="Title" required type="text"  value={this.state.title} name="title" id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis" className="form-control">Synopsis</label>
                <textarea onChange={this.handleSynopsisChange} id="synopsis" rows="3"  value={this.state.synopsis} name="synopsis" className="form-control"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={this.handleConferenceChange} required id="conference"  value={this.state.conference} name="conference" className="form-select">
                    <option value="">Choose a conference</option>
                    {this.state.conferences.map(conference => {
                      return (
                        <option id={conference.id} key={conference.href} value={conference.name}>
                          {conference.name}
                        </option>
                      );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default PresentationForm;