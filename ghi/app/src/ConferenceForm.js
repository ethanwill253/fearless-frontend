import React from 'react';

class ConferenceForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      starts: '',
      ends: '',
      description: '',
      maxPresentations: '',
      maxAttendees: '',
      locations: []
    };

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleStartsChange = this.handleStartsChange.bind(this);
    this.handleEndsChange = this.handleEndsChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handleMaxPresentChange = this.handleMaxPresentChange.bind(this);
    this.handleMaxAttendChange = this.handleMaxAttendChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleNameChange(event) {
    const value = event.target.value;
    this.setState({name: value})
  }

  handleStartsChange(event) {
    // const value = event.target.value;
    const value = event.target.value;
    // this.setState({starts: value})
    this.setState({starts: value})
  }

  handleEndsChange(event) {
    // const value = event.target.value;
    const value = event.target.value;
    // this.setState({ends: value})
    this.setState({ends: value})
  }

  handleDescriptionChange(event) {
    const value = event.target.value;
    this.setState({description: value})
  }

  handleMaxPresentChange(event) {
    const value = event.target.value;
    this.setState({maxPresentations: value})
  }

  handleMaxAttendChange(event) {
    const value = event.target.value;
    this.setState({maxAttendees: value})
  }

  handleLocationChange(event) {
    const value = event.target.value;
    this.setState({location: value})
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    data.max_presentations = data.maxPresentations;
    data.max_attendees = data.maxAttendees;
    delete data.maxPresentations;
    delete data.maxAttendees;
    delete data.locations;
    console.log("Submit data: ",data);

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
          'Content-Type': 'application/json',
      },
    };
    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log("New Conference data: ",newConference);
      const cleared = {
        name: '',
        starts: '',
        ends: '',
        description: '',
        maxPresentations: '',
        maxAttendees: '',
        locations: []
      };
      this.setState(cleared);
    }
  }

  async componentDidMount() {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        this.setState({locations: data.locations})
        // const selectTag = document.getElementById('location');
        // for (let location of data.locations) {
        //     const option = document.createElement('option')
        //     option.innerHTML = location['name']
        //     option.value = location['id']
        //     selectTag.appendChild(option)
        // }
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleNameChange} placeholder="Name" required type="text"  value={this.state.name} name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleStartsChange} placeholder="Start date" required  type="date" value={this.state.starts} name="starts" id="starts" className="form-control" />
                <label htmlFor="starts">Start date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleEndsChange} placeholder="End date" required  type="date"  value={this.state.ends} name="ends" id="ends" className="form-control" />
                <label htmlFor="ends">End date</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description" className="form-label">Description</label>
                <textarea onChange={this.handleDescriptionChange} className="form-control" value={this.state.description} name="description" id="description" rows="3"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleMaxPresentChange} placeholder="Max presentations" required type="number" value={this.state.maxPresentations} name="max_presentations" id="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Max presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleMaxAttendChange} placeholder="Max attendees" required type="number" value={this.state.maxAttendees} name="max_attendees" id="max_attendees" className="form-control" />
                <label htmlFor="max_attendees">Max attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleLocationChange} required id="location" value={this.state.location} name="location" className="form-select">
                    <option value="">Choose a location</option>
                    {this.state.locations.map(location => {
                      return (
                        <option key={location.id} value={location.id}>
                          {location.name}
                        </option>
                      );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ConferenceForm;